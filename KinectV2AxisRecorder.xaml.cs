﻿namespace KinectV2AxisRecorder
{

    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Drawing;
    using System.Globalization;
    using System.IO;
    using System.IO.Pipes;
    using System.Runtime.InteropServices;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Xml.Serialization;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Forms;
    using Microsoft.Kinect;
    using Emgu.CV;
    using Emgu.CV.Structure;
    using Ookii.Dialogs.Wpf;


    using CheckBox = System.Windows.Controls.CheckBox;

    /// <summary>
    /// Interaction logic for MainWindow
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        /// <summary>
        /// Active Kinect sensor
        /// </summary>
        private KinectSensor kinectSensor = null;

        /// <summary>
        /// Stopwatch for adjusting the frame rate
        /// </summary>
        private Stopwatch t0 = new Stopwatch();


        /// <summary>
        /// Minimum gap between frames, in ms, in order to keep the desired frame rate
        /// </summary>
        private int desiredGapBetweenFrames;
        private int desiredFrameRate;


        /// <summary>
        /// Reader for depth/color frames
        /// </summary>
        private MultiSourceFrameReader reader = null;

        /// <summary>
        /// Bitmap to display
        /// </summary>
        private WriteableBitmap depthBitmap = null;
        private WriteableBitmap colorBitmap = null;
        private byte[] colorPixels = null;

        /// <summary>
        /// Array for the bodies
        /// </summary>
        private Body[] bodies = null;

        /// <summary>
        /// Bitmaps to write the registration look up tables
        /// </summary>
        private WriteableBitmap colorToDepthMapX = null;
        private WriteableBitmap colorToDepthMapY = null;
        private WriteableBitmap depthToColorMapX = null;
        private WriteableBitmap depthToColorMapY = null;

        /// <summary>
        /// Coordinate mapper to map one type of point to another
        /// </summary>
        private CoordinateMapper coordinateMapper = null;

        /// <summary>
        /// Current status text to display
        /// </summary>
        private string statusText = null;
        private string statusText2 = null;

        /// <summary>
        /// Size of the RGB pixel in the bitmap
        /// </summary>
        private readonly int cbytesPerPixel = (PixelFormats.Gray16.BitsPerPixel + 7) / 8;
        private readonly int bytesPerColorPixel = (PixelFormats.Bgr32.BitsPerPixel + 7) / 8;

        /// <summary>
        /// Flag deciding if we are recording or not
        /// </summary>
        private bool isRecording;

        /// <summary>
        /// Flag deciding if we want to capture thermal images
        /// </summary>
        private bool captureThermalStream;

        /// <summary>
        /// Flag deciding if we stop capture if the Kinect is disconnected
        /// </summary>
        private bool stopCaptureAtConnLoss;

        /// <summary>
        /// Flag deciding if we automatically validate each sequence after capture
        /// </summary>
        private bool validateFramesAfterCapture;

        /// <summary>
        /// Flag deciding if we capture the body stream
        /// </summary>
        private bool captureBodyStream;

        /// <summary>
        /// Flag deciding if we capture the depth stream
        /// </summary>
        private bool captureDepthStream;

        /// <summary>
        /// Flag deciding if we use the timestamp of the frames or a global frame count
        /// </summary>
        private bool useTimestampNaming;

        private long frameCount;

        private int depthWidth;
        private int depthHeight;
        private int colorWidth;
        private int colorHeight;

        private List<KinectV2AxisRecorder.FrameBuffer> frameBuffers;
        //private DepthBasicsWPF.FrameBuffer frameBuffer;
        CyclicCounter activeBuffer; 
        private int bufferCount;

        /// <summary>
        /// Timers to write files to disk
        /// </summary>
        List<System.Threading.Timer> writeToDiskTimer;
        List<string> discardedFrames;

        private string captureBaseFolder;
        private int sequenceNbr;
        
        // Processes and handles for the Thermal Camera communication
        Process thermalClient;
        AnonymousPipeServerStream pipeSender;
        AnonymousPipeServerStream pipeReceiver;

        // The pipeMessageReceiver is a pipe that is reserved for receiving
        // exception messages from the thermal client, e.g. if the connection
        // to the camera is suddenly lost. 
        AnonymousPipeServerStream pipeExceptionMessageReceiver;

        StreamWriter pipeWriter;
        StreamReader pipeReader;
        Thread thermalMessageThread;

        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// </summary>
        public MainWindow()
        {  
            // for Alpha, one sensor is supported
            this.kinectSensor = KinectSensor.GetDefault();
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-us");

            useTimestampNaming = true;
            frameCount = 0;

            if (this.kinectSensor != null)
            {
                // get the coordinate mapper
                this.coordinateMapper = this.kinectSensor.CoordinateMapper;

                //start clock
                this.t0.Start();
                this.desiredGapBetweenFrames = 30;

                // open the sensor
                this.kinectSensor.Open();

                FrameDescription depthFrameDescription = this.kinectSensor.DepthFrameSource.FrameDescription;

                // create the depth bitmap
                this.depthBitmap = new WriteableBitmap(depthFrameDescription.Width, depthFrameDescription.Height, 96.0, 96.0, PixelFormats.Gray16, null);

                FrameDescription colorFrameDescription = this.kinectSensor.ColorFrameSource.FrameDescription;

                int colorWidth = colorFrameDescription.Width;
                int colorHeight = colorFrameDescription.Height;
                this.depthWidth = depthFrameDescription.Width;
                this.depthHeight = depthFrameDescription.Height;
                this.colorHeight = colorFrameDescription.Height;
                this.colorWidth = colorFrameDescription.Width;

                // Set the default capture flags
                captureBodyStream = true;
                captureDepthStream = true;

                // create the color bitmap
                this.colorBitmap = new WriteableBitmap(colorFrameDescription.Width, colorFrameDescription.Height, 96.0, 96.0, PixelFormats.Bgra32, null);

                // create the bitmap for the look up tables
                this.colorToDepthMapX = new WriteableBitmap(colorFrameDescription.Width, colorFrameDescription.Height, 96.0, 96.0, PixelFormats.Gray16, null);
                this.colorToDepthMapY = new WriteableBitmap(colorFrameDescription.Width, colorFrameDescription.Height, 96.0, 96.0, PixelFormats.Gray16, null);
                this.depthToColorMapX = new WriteableBitmap(depthFrameDescription.Width, depthFrameDescription.Height, 96.0, 96.0, PixelFormats.Gray16, null);
                this.depthToColorMapY = new WriteableBitmap(depthFrameDescription.Width, depthFrameDescription.Height, 96.0, 96.0, PixelFormats.Gray16, null);

                // Allocate buffers
                int bufferLength = 150;
                bufferCount = 2;
                this.colorPixels = new byte[colorFrameDescription.Width * colorFrameDescription.Height * this.bytesPerColorPixel];
                frameBuffers = new List<FrameBuffer>();

                writeToDiskTimer = new List<System.Threading.Timer>();

                for (int i = 0; i < bufferCount; ++i)
                {
                    FrameBuffer buf = new FrameBuffer();
                    buf.init(this.captureBaseFolder, colorWidth, colorHeight, depthHeight, depthWidth, 4, bufferLength, captureDepthStream, captureBodyStream, false);
                    frameBuffers.Add(buf);

                    // Create the timer that is responsible for saving the frames on the disk
                    System.Threading.TimerCallback tcb = frameBuffers[i].save;
                    System.Threading.Timer tmr = new System.Threading.Timer(tcb, null, 0, Timeout.Infinite);
                    writeToDiskTimer.Add(tmr);
                }

                // Construct the activeBuffer, the cyclic counter keeping track of which buffer is currently in use
                activeBuffer = new CyclicCounter(bufferCount);
                discardedFrames = new List<string>();
                
                // open the reader for the depth and color frames
                this.reader = this.kinectSensor.OpenMultiSourceFrameReader(FrameSourceTypes.Depth | FrameSourceTypes.Color | FrameSourceTypes.Body);

                // set the status text
                this.StatusText = Properties.Resources.InitializingStatusTextFormat;

                // set IsAvailableChanged event notifier
                this.kinectSensor.IsAvailableChanged += this.Sensor_IsAvailableChanged;
        
                // Initialize the sequence number
                this.sequenceNbr = 1;
                this.captureBaseFolder = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments).ToString() + "\\Capture";
            }
            else
            {
                // on failure, set the status text
                this.StatusText = Properties.Resources.NoSensorStatusText;
            }
            
            // use the window object as the view model in this simple example
            this.DataContext = this;

            // initialize the components (controls) of the window
            this.InitializeComponent();

            this.SeqNbrBox.Text = this.sequenceNbr.ToString();
            this.pathBox.Text = this.captureBaseFolder;

            // Set default flags
            isRecording = false;
            stopCaptureAtConnLoss = true;
            validateFramesAfterCapture = true;
            captureThermalStream = false;

            this.progressBar.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// INotifyPropertyChangedPropertyChanged event to allow window controls to bind to changeable data
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets the bitmap to display
        /// </summary>
        public ImageSource ImageSource
        {
            get
            {
                return this.colorBitmap;// this.bitmap;
            }
        }

        /// <summary>
        /// Gets or sets the current status text to display
        /// </summary>
        public string StatusText
        {
            get
            {
                return this.statusText;
            }

            set
            {
                if (this.statusText != value)
                {
                    this.statusText = value;

                    // notify any bound elements that the text has changed
                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(this, new PropertyChangedEventArgs("StatusText"));
                    }
                }
            }
        }

        public string StatusText2
        {
            get
            {
                return this.statusText2;
            }

            set
            {
                if (this.statusText2 != value)
                {
                    this.statusText2 = value;

                    // notify any bound elements that the text has changed
                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(this, new PropertyChangedEventArgs("StatusText2"));
                    }
                }
            }
        }

        /// <summary>
        /// Execute start up tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.reader != null)
            {
                this.reader.MultiSourceFrameArrived += this.Reader_MultiSourceFrameArrived;            
            }

            // Set the appropiate text on the buttons
            this.captureRgbDButton.Content = getCaptureButtonText("Capture ", isRecording);

        }

        /// <summary>
        /// Execute shutdown tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            if (this.reader != null)
            {
                // MultiSourceFrameReder is IDisposable
                this.reader.Dispose();
                this.reader = null;
            }

            if (this.kinectSensor != null)
            {
                this.kinectSensor.Close();
                this.kinectSensor = null;
            }

            string path = "discardedFrames.txt";

            if (discardedFrames.Count > 1)
            {
                File.AppendAllLines(path, discardedFrames);
            }

            if (captureThermalStream)
            {
                try
                {
                    // Close the thermal camera
                    thermalClient.CloseMainWindow();
                    thermalClient.Kill();
                    pipeSender.Dispose();
                    pipeReceiver.Dispose();
                    pipeExceptionMessageReceiver.Dispose();
                }
                catch (Exception exp)
                {
                    Console.WriteLine(exp.Message);
                }
            }
        }

        private void StartThermalClient()
        {
            // Initialize the thermal client
            thermalClient = new Process();
            thermalClient.StartInfo.FileName = "AxisRecorder.exe";

            // Create pipes
            pipeSender = new AnonymousPipeServerStream(PipeDirection.Out,
                            HandleInheritability.Inheritable);
            pipeReceiver = new AnonymousPipeServerStream(PipeDirection.In,
                            HandleInheritability.Inheritable);
            pipeExceptionMessageReceiver = new AnonymousPipeServerStream(PipeDirection.In, 
                            HandleInheritability.Inheritable);

            Console.WriteLine("[Kinect] Starting communication with thermal client");

            try
            {
                // Pass the thermal client process a handle to the server
                thermalClient.StartInfo.Arguments =
                    pipeSender.GetClientHandleAsString() + " " +
                    pipeReceiver.GetClientHandleAsString() + " " +
                    pipeExceptionMessageReceiver.GetClientHandleAsString();
                thermalClient.StartInfo.UseShellExecute = false;
                thermalClient.Start();

                // Release resources handled by client
                pipeSender.DisposeLocalCopyOfClientHandle();
                pipeReceiver.DisposeLocalCopyOfClientHandle();
                pipeExceptionMessageReceiver.DisposeLocalCopyOfClientHandle();
                
            }
            catch (Exception e)
            {
                Console.WriteLine("[Kinect] Error: {0}", e.Message);

            }
            
            pipeReader = new StreamReader(pipeReceiver, System.Text.Encoding.Default, false, 1024, true);
            pipeWriter = new StreamWriter(pipeSender, System.Text.Encoding.Default, 1024, true);  
            pipeWriter.AutoFlush = true;
            pipeWriter.WriteLine("SYNC");
            pipeSender.WaitForPipeDrain();
            
            string message = pipeReader.ReadLine();
            if (message == "SYNCKINECT-connected")
            {
                Console.WriteLine("[Kinect] Connection established");
                ChangeThermalPath();
            }
            else
            {
                Console.WriteLine("[Kinect] Connection failed, received " + message);
            }

            // Start the message parser
            thermalMessageThread = new Thread(() => ThermalMessageReceiver());
            thermalMessageThread.Start();
        }

        private void ChangeThermalPath()
        {
            string fullPath = this.captureBaseFolder + "\\" + this.SeqNbrBox.Text + "\\";

            try
            {                             
                    // Ask the thermal client to adjust its file path
                    Console.WriteLine("[Kinect] Attempting to change the thermal path to {0}...", fullPath);
                    string messageStr = "SYNCAXIS-path";
                    pipeWriter.WriteLine(messageStr);
                    pipeSender.WaitForPipeDrain();
                    string received = pipeReader.ReadLine();
                    Console.WriteLine("[Kinect] Connection established. Received " + received);
                    
                    // Send the path to the thermal camera
                    pipeWriter.WriteLine(fullPath);
                    pipeSender.WaitForPipeDrain();
                    received = pipeReader.ReadLine();
                    Console.WriteLine("[Kinect] Received " + received);

                    // Send a superflous message to get the latest status from the client
                    pipeWriter.WriteLine("Waiting");
                    pipeSender.WaitForPipeDrain();

                    received = pipeReader.ReadLine();
                    Console.WriteLine("[Kinect] ...Done. Received " + received);

            }
            catch (IOException e)
            {
                Console.WriteLine("[Kinect] Error: {0}", e.Message);
            }

        }


        /// <summary>
        /// The receiver is listening to and parsing messages from the thermal client
        /// when we are not actively sending messages to the thermal camera, such as 
        /// start/stop, path change, etc. The purpose of the message receiver is to 
        /// understand and act on abrupt messages, such as connection loss.  
        /// </summary>
        private void ThermalMessageReceiver()
        {
            StreamReader exceptionReader = new StreamReader(pipeExceptionMessageReceiver, 
                System.Text.Encoding.Default, false, 1024, true);
            string receivedMessage;

            while (true)
            {
                try
                {
                    receivedMessage = exceptionReader.ReadLine();
                    Console.WriteLine("[Message parser]: " + receivedMessage);

                    if (receivedMessage.Contains("ERROR"))
                    {
                        Dispatcher.Invoke(delegate()
                        {
                            if (isRecording)
                            {
                                // on failure, set the status text
                                this.statusText2 = "Capture stopped abruptly";
                                StartStopCapture();

                                // Show messagebox that we have closed the recording
                                System.Windows.MessageBoxResult result = System.Windows.MessageBox.Show("The current capture has ended due to connection loss of the Thermal sensor",
                                "Thermal camera unavailable", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Asterisk);
                            }
                        });
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error communicating with the thermal camera. Shutting camera down. Error: " + e.Message);
                    Dispatcher.Invoke(delegate()
                    {
                        // Close the thermal client
                        CloseThermalClient();
                    });
                    return;
                }
                
                Thread.Sleep(500);
            }
            
        }

        private bool StartThermalRecording()
        {
            try
            {
                // Ask the thermal client to adjust its file path
                Console.WriteLine("[Kinect] Attempting to start the thermal recording");
                string messageStr = "SYNCAXIS-start";
                pipeWriter.WriteLine(messageStr);
                pipeSender.WaitForPipeDrain();
                Console.WriteLine("[Kinect] Message received at thermal camera");

                // Send a superflous message to wait for the client
                pipeWriter.WriteLine("Waiting");
                pipeSender.WaitForPipeDrain();
                Console.WriteLine("[Kinect] Wait message sent to thermal");

                string recieved = pipeReader.ReadLine();
                Console.WriteLine(recieved);

                if (("ECHO-" + messageStr) == recieved)
                {
                    Console.WriteLine("[Kinect] Thermal recording started");
                }
                else
                {
                    throw new Exception("Thermal recording could not be started. Recieved " + recieved);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("[Kinect] Error: {0}", e.Message);
                return false;
            }

            return true;
        }

        private bool StopThermalRecording()
        {
            try
            {
                // Ask the thermal client to adjust its file path
                Console.WriteLine("[Kinect] Attempting to stop the thermal recording");
                string messageStr = "SYNCAXIS-stop";
                pipeWriter.WriteLine(messageStr);
                pipeSender.WaitForPipeDrain();
                Console.WriteLine("[Kinect] Message received at thermal camera");

                // Send a superflous message to wait for the client
                pipeWriter.WriteLine("Waiting");
                Console.WriteLine("[Kinect] Wait message sent to thermal");
                pipeSender.WaitForPipeDrain();

                string recieved = pipeReader.ReadLine();

                if (("ECHO-" + messageStr) == recieved)
                {
                    Console.WriteLine("[Kinect] Thermal recording stopped");
                }
                else
                {
                    throw new Exception("Thermal recording could not be stopped. Recieved " + recieved);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("[Kinect] Error: {0}", e.Message);
                return false;
            }

            return true;
        }

        static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }

        static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
            
            
        private void coordinateMapper_CoordinateMappingChanged()
        {
            progressBar.Maximum = 100;
            StatusText2 = "Initializing look-up tables";

            
            this.coordinateMapper = this.kinectSensor.CoordinateMapper;

            FrameDescription depthFrameDescription = this.kinectSensor.DepthFrameSource.FrameDescription;
            FrameDescription colorFrameDescription = this.kinectSensor.ColorFrameSource.FrameDescription;

            DepthFrame depthFrame = this.kinectSensor.DepthFrameSource.OpenReader().AcquireLatestFrame();
            ColorFrame colorFrame = this.kinectSensor.ColorFrameSource.OpenReader().AcquireLatestFrame();

            int colorWidth = colorFrameDescription.Width;
            int colorHeight = colorFrameDescription.Height;
            int depthWidth = depthFrameDescription.Width;
            int depthHeight = depthFrameDescription.Height;

            ColorSpacePoint[] depthToColorPoints = new ColorSpacePoint[depthWidth * depthHeight];
            DepthSpacePoint[] colorToDepthPoints = new DepthSpacePoint[colorWidth * colorHeight];
            Microsoft.Kinect.PointF[] depthToCameraTable = new Microsoft.Kinect.PointF[depthWidth * depthHeight];
            CameraSpacePoint[] depthToCameraPoints = new CameraSpacePoint[depthWidth * depthHeight];

                
            ushort[] depthFrameData = new ushort[depthFrameDescription.Width * depthFrameDescription.Height];
            byte[] colorFrameData = new byte[colorFrameDescription.Width  * colorFrameDescription.Height * this.bytesPerColorPixel];

            if (colorFrame.RawColorImageFormat == ColorImageFormat.Bgra)
            {
                colorFrame.CopyRawFrameDataToArray(colorFrameData);
            }
            else
            {
                colorFrame.CopyConvertedFrameDataToArray(colorFrameData, ColorImageFormat.Bgra);
            }

            depthFrame.CopyFrameDataToArray(depthFrameData);

            this.coordinateMapper.MapDepthFrameToColorSpace(depthFrameData, depthToColorPoints);
            this.coordinateMapper.MapColorFrameToDepthSpace(depthFrameData, colorToDepthPoints);
            depthToCameraTable = this.coordinateMapper.GetDepthFrameToCameraSpaceTable();
            this.coordinateMapper.MapDepthFrameToCameraSpace(depthFrameData, depthToCameraPoints);           

            // Depth to color lookup
            ushort[] depthToColorLookupX = new ushort[depthWidth * depthHeight];
            ushort[] depthToColorLookupY = new ushort[depthWidth * depthHeight];


            progressBar.Value = 15;


            for (int i = 0; i < depthToColorPoints.Length; ++i)
            {
                int colorX = (int)Math.Abs(Math.Floor(depthToColorPoints[i].X + 0.5));
                int colorY = (int)Math.Abs(Math.Floor(depthToColorPoints[i].Y + 0.5));

                if ((colorX >= 0) && (colorX < colorWidth) && (colorY >= 0) && (colorY < colorHeight))
                {
                    depthToColorLookupX[i] = (ushort)colorX;
                    depthToColorLookupY[i] = (ushort)colorY;
                }
            }

            progressBar.Value = 40;

            // Depth to camera space lookup
            string[] depthToCameraLookupX = new string[depthWidth * depthHeight];
            string[] depthToCameraLookupY = new string[depthWidth * depthHeight];
            string[] depthToCameraLookupZ = new string[depthWidth * depthHeight];

            for (int i = 0; i < depthToCameraPoints.Length; ++i)
            {
                depthToCameraLookupX[i] = depthToCameraPoints[i].X.ToString();
                depthToCameraLookupY[i] = depthToCameraPoints[i].Y.ToString();
                depthToCameraLookupZ[i] = depthToCameraPoints[i].Z.ToString();
            }

            progressBar.Value = 60;

            // Color to depth lookup
            ushort[] colorToDepthLookupX = new ushort[colorWidth * colorHeight];
            ushort[] colorToDepthLookupY = new ushort[colorWidth * colorHeight];

            for (int i = 0; i < colorToDepthPoints.Length; ++i)
            {
                int depthX = (int)Math.Floor(colorToDepthPoints[i].X + 0.5);
                int depthY = (int)Math.Floor(colorToDepthPoints[i].Y + 0.5);

                if ((depthX >= 0) && (depthX < depthWidth) && (depthY >= 0) && (depthY < depthHeight))
                {
                    colorToDepthLookupX[i] = (ushort)depthX;
                    colorToDepthLookupY[i] = (ushort)depthY;
                }
            }

            progressBar.Value = 70;

            string[] depthToCameraLookupVectorX = new string[depthWidth * depthHeight];
            string[] depthToCameraLookupVectorY = new string[depthWidth * depthHeight];

            for (int i = 0; i < depthToCameraTable.Length; ++i)
            {
                depthToCameraLookupVectorX[i] = depthToCameraTable[i].X.ToString();
                depthToCameraLookupVectorY[i] = depthToCameraTable[i].Y.ToString();
            }

            progressBar.Value = 80;

            // Write bitmapls
            this.depthToColorMapX.WritePixels(
            new Int32Rect(0, 0, depthWidth, depthHeight),
            depthToColorLookupX,
            depthWidth * this.cbytesPerPixel,
            0);

            string basePath = this.captureBaseFolder + "\\" + this.sequenceNbr.ToString() + "\\Lookup\\Generic\\";
            string mapPath;

            BitmapEncoder encoder1 = new PngBitmapEncoder();
            encoder1.Frames.Add(BitmapFrame.Create(this.depthToColorMapX));
            mapPath = basePath + "D2RGB-X.png";

            using (FileStream fs = new FileStream(mapPath, FileMode.Create))
                encoder1.Save(fs);

            this.depthToColorMapY.WritePixels(
            new Int32Rect(0, 0, depthWidth, depthHeight),
            depthToColorLookupY,
            depthWidth * this.cbytesPerPixel,
            0);

            BitmapEncoder encoder2 = new PngBitmapEncoder();
            encoder2.Frames.Add(BitmapFrame.Create(this.depthToColorMapY));
            mapPath = basePath + "D2RGB-Y.png";

            using (FileStream fs = new FileStream(mapPath, FileMode.Create))
                encoder2.Save(fs);

            this.colorToDepthMapX.WritePixels(
            new Int32Rect(0, 0, colorWidth, colorHeight),
            colorToDepthLookupX,
            colorWidth * this.cbytesPerPixel,
            0);

            BitmapEncoder encoder3 = new PngBitmapEncoder();
            encoder3.Frames.Add(BitmapFrame.Create(this.colorToDepthMapX));
            mapPath = basePath + "RGB2D-X.png";

            using (FileStream fs = new FileStream(mapPath, FileMode.Create))
                encoder3.Save(fs);

            this.colorToDepthMapY.WritePixels(
            new Int32Rect(0, 0, colorWidth, colorHeight),
            colorToDepthLookupY,
            colorWidth * this.cbytesPerPixel,
            0);

            BitmapEncoder encoder4 = new PngBitmapEncoder();
            encoder4.Frames.Add(BitmapFrame.Create(this.colorToDepthMapY));
            mapPath = basePath + "RGB2D-Y.png";

            using (FileStream fs = new FileStream(mapPath, FileMode.Create))
                encoder4.Save(fs);

            progressBar.Value = 90;


            mapPath = basePath + "DepthFrameToCameraSpaceVectorX.txt";
            System.IO.File.WriteAllLines(mapPath, depthToCameraLookupVectorX);

            mapPath = basePath + "DepthFrameToCameraSpaceVectorY.txt";
            System.IO.File.WriteAllLines(mapPath, depthToCameraLookupVectorY);

            mapPath = basePath + "DepthFrameToCameraSpaceX.txt";
            System.IO.File.WriteAllLines(mapPath, depthToCameraLookupX);

            mapPath = basePath + "DepthFrameToCameraSpaceY.txt";
            System.IO.File.WriteAllLines(mapPath, depthToCameraLookupY);

            mapPath = basePath + "DepthFrameToCameraSpaceZ.txt";
            System.IO.File.WriteAllLines(mapPath, depthToCameraLookupZ);         
   
            // Write the corresponding depth image to the lookup table
            BitmapEncoder depthEncoder = new PngBitmapEncoder();

            WriteableBitmap depthBitmap = null;
            depthBitmap = new WriteableBitmap(depthWidth, depthHeight, 96.0, 96.0, PixelFormats.Gray16, null);
            mapPath = basePath + "ReferenceD.png";

            depthBitmap.WritePixels(
                new Int32Rect(0, 0, depthWidth, depthHeight),
                depthFrameData,
                depthWidth * this.cbytesPerPixel,
                0);

            progressBar.Value = 99;

            depthEncoder.Frames.Add(BitmapFrame.Create(depthBitmap));

            using (FileStream fs = new FileStream(mapPath, FileMode.Create))
            {
                depthEncoder.Save(fs);
            }

            if (depthFrame != null)
            {
                depthFrame.Dispose();
                depthFrame = null;
            }

            if (colorFrame != null)
            {
                colorFrame.Dispose();
                colorFrame = null;
            }
                        
        }

        /// <summary>
        /// Generates the lookup tables for the registration from color <-> depth based on the images in the provided directory
        /// </summary>
        /// <param name="baseDir"></param>
        private void generateSpecificRegistrationLookupTables(string baseDir, int depthWidth, int depthHeight, int colorWidth, int colorHeight,
                                                                int cbytesPerPixel)
        {
            
            // Find suitable files in both the depth and color directories
            string depthpath = baseDir + "\\D\\";
            string colorpath = baseDir + "\\RGB\\";
            string[] depthImages;
            string[] colorImages;

            // Fetch  the coordinateMapper
            this.coordinateMapper = this.kinectSensor.CoordinateMapper;

            Dispatcher.Invoke(delegate()
            {
                progressBar.Value = 0;
            });
            
            depthImages = Directory.GetFiles(depthpath, "*.png");
            Dispatcher.Invoke(delegate()
            {
                progressBar.Value = 0.5;
            });

            colorImages = Directory.GetFiles(colorpath, "*.jpg");
            Dispatcher.Invoke(delegate()
            {
                progressBar.Value = 1;
            });
            

            string lookupdir = baseDir + "\\Lookup\\";

            Dispatcher.Invoke(delegate() {
                progressBar.Maximum = depthImages.Length-1;
                StatusText2 = "Generating registration look-up tables";
                });

            /// <summary>
            /// Bitmaps to write the registration look up tables
            /// </summary>
            WriteableBitmap colorToDepthMap;
            WriteableBitmap depthToColorMap;

            colorToDepthMap = new WriteableBitmap(colorWidth, colorHeight, 96.0, 96.0, PixelFormats.Gray16, null);
            depthToColorMap = new WriteableBitmap(depthWidth, depthHeight, 96.0, 96.0, PixelFormats.Gray16, null);

            for (int imgNbr = 0; imgNbr < depthImages.Length; ++imgNbr)
            {
                ushort[] depthValues = new ushort[depthWidth * depthHeight];
                Emgu.CV.Image<Gray, UInt16> img = new Image<Gray, UInt16>(depthImages[imgNbr]);
                // Copy data from a 3D array into a 1D array
                System.Buffer.BlockCopy(img.Data, 0, depthValues, 0, depthWidth * depthHeight * 2);
                
                // Generate the look-up tables for the images
                ColorSpacePoint[] depthToColorPoints = new ColorSpacePoint[depthWidth * depthHeight];
                DepthSpacePoint[] colorToDepthPoints = new DepthSpacePoint[colorWidth * colorHeight];

                this.coordinateMapper.MapColorFrameToDepthSpace(depthValues, colorToDepthPoints);
                this.coordinateMapper.MapDepthFrameToColorSpace(depthValues, depthToColorPoints);

                string depthFileName = System.IO.Path.GetFileNameWithoutExtension(depthImages[imgNbr]);
                string colorFileName = System.IO.Path.GetFileNameWithoutExtension(colorImages[imgNbr]);

                saveRegistrationTablesAsync(lookupdir, depthFileName, colorFileName, colorToDepthPoints, depthToColorPoints,
                                            colorWidth, colorHeight, depthWidth, depthHeight, cbytesPerPixel,
                                            colorToDepthMap, depthToColorMap);
                Dispatcher.Invoke(delegate() {
                    progressBar.Value = imgNbr;
                });
            }

        }

        /// <summary>
        /// Generates generic look-up tables of fixed depth
        /// </summary>
        private void generateGenericRegistrationLookupTables(string baseDir, int depthWidth, int depthHeight, int colorWidth, int colorHeight,
                                                                int cbytesPerPixel)
                                                             
        {
            this.coordinateMapper = this.kinectSensor.CoordinateMapper;

            ushort minDepth = 500;
            ushort maxDepth = 8100;

            Dispatcher.Invoke(delegate()
            {
                progressBar.Maximum = maxDepth - minDepth;
                StatusText2 = "Generating generic look-up tables";
            });

            ushort[] depthValues = new ushort[depthWidth * depthHeight];

            string genericLookupDir = baseDir + "\\Lookup\\Generic\\";

                        /// <summary>
            /// Bitmaps to write the registration look up tables
            /// </summary>
            WriteableBitmap colorToDepthMap;
            WriteableBitmap depthToColorMap;

            colorToDepthMap = new WriteableBitmap(colorWidth, colorHeight, 96.0, 96.0, PixelFormats.Gray16, null);
            depthToColorMap = new WriteableBitmap(depthWidth, depthHeight, 96.0, 96.0, PixelFormats.Gray16, null);

            for (ushort i = minDepth; i < maxDepth; ++i)
            {
                for (int j = 0; j < depthValues.Length; ++j)
                {
                    depthValues[j] = i;
                }

                // Generate the look-up tables for this specific depth
                ColorSpacePoint[] depthToColorPoints = new ColorSpacePoint[depthWidth * depthHeight];
                DepthSpacePoint[] colorToDepthPoints = new DepthSpacePoint[colorWidth * colorHeight];

                this.coordinateMapper.MapColorFrameToDepthSpace(depthValues, colorToDepthPoints);
                this.coordinateMapper.MapDepthFrameToColorSpace(depthValues, depthToColorPoints);

                string depthFileName = "D2RGB-" + i.ToString("D5");
                string colorFileName = "RGB2D-" + i.ToString("D5");

                saveRegistrationTablesAsync(genericLookupDir, depthFileName, colorFileName, colorToDepthPoints, depthToColorPoints,
                                            colorWidth, colorHeight, depthWidth, depthHeight, cbytesPerPixel, colorToDepthMap, 
                                            depthToColorMap);

                Dispatcher.Invoke(delegate()
                {
                    progressBar.Value = i - minDepth;
                });
            }
        }

        private void saveRegistrationTablesAsync(string saveDir, string depthFileName, string colorFileName, 
                                            DepthSpacePoint[] colorToDepthPoints, ColorSpacePoint[] depthToColorPoints, 
                                            int colorWidth, int colorHeight, int depthWidth, int depthHeight, int cbytesPerPixel,
                                            WriteableBitmap colorToDepthMap, WriteableBitmap depthToColorMap)
        {
            // Depth to color lookup
            ushort[] depthToColorLookupX = new ushort[depthWidth * depthHeight];
            ushort[] depthToColorLookupY = new ushort[depthWidth * depthHeight];

            for (int i = 0; i < depthToColorPoints.Length; ++i)
            {
                int colorX = (int)Math.Abs(Math.Floor(depthToColorPoints[i].X + 0.5));
                int colorY = (int)Math.Abs(Math.Floor(depthToColorPoints[i].Y + 0.5));

                if ((colorX >= 0) && (colorX < colorWidth) && (colorY >= 0) && (colorY < colorHeight))
                {
                    depthToColorLookupX[i] = (ushort)colorX;
                    depthToColorLookupY[i] = (ushort)colorY;
                }
            }

            // Color to depth lookup
            ushort[] colorToDepthLookupX = new ushort[colorWidth * colorHeight];
            ushort[] colorToDepthLookupY = new ushort[colorWidth * colorHeight];

            for (int i = 0; i < colorToDepthPoints.Length; ++i)
            {
                int depthX = (int)Math.Floor(colorToDepthPoints[i].X + 0.5);
                int depthY = (int)Math.Floor(colorToDepthPoints[i].Y + 0.5);

                if ((depthX >= 0) && (depthX < depthWidth) && (depthY >= 0) && (depthY < depthHeight))
                {
                    colorToDepthLookupX[i] = (ushort)depthX;
                    colorToDepthLookupY[i] = (ushort)depthY;
                }
            }

            string finalPath;

            depthToColorMap.WritePixels(
            new Int32Rect(0, 0, depthWidth, depthHeight),
            depthToColorLookupX,
            depthWidth * cbytesPerPixel,
            0);

            BitmapEncoder encoder1 = new PngBitmapEncoder();
            encoder1.Frames.Add(BitmapFrame.Create(depthToColorMap));
            finalPath = saveDir + depthFileName + "-X.png";

            using (FileStream fs = new FileStream(finalPath, FileMode.Create))
                encoder1.Save(fs);

            depthToColorMap.WritePixels(
            new Int32Rect(0, 0, depthWidth, depthHeight),
            depthToColorLookupY,
            depthWidth * cbytesPerPixel,
            0);

            BitmapEncoder encoder2 = new PngBitmapEncoder();
            encoder2.Frames.Add(BitmapFrame.Create(depthToColorMap));
            finalPath = saveDir + depthFileName + "-Y.png";

            using (FileStream fs = new FileStream(finalPath, FileMode.Create))
                encoder2.Save(fs);

            colorToDepthMap.WritePixels(
            new Int32Rect(0, 0, colorWidth, colorHeight),
            colorToDepthLookupX,
            colorWidth * cbytesPerPixel,
            0);

            BitmapEncoder encoder3 = new PngBitmapEncoder();
            encoder3.Frames.Add(BitmapFrame.Create(colorToDepthMap));
            finalPath = saveDir + colorFileName + "-X.png";

            using (FileStream fs = new FileStream(finalPath, FileMode.Create))
                encoder3.Save(fs);

            colorToDepthMap.WritePixels(
            new Int32Rect(0, 0, colorWidth, colorHeight),
            colorToDepthLookupY,
            colorWidth * this.cbytesPerPixel,
            0);

            BitmapEncoder encoder4 = new PngBitmapEncoder();
            encoder4.Frames.Add(BitmapFrame.Create(colorToDepthMap));
            finalPath = saveDir + colorFileName + "-Y.png";

            using (FileStream fs = new FileStream(finalPath, FileMode.Create))
                encoder4.Save(fs);

           
            colorToDepthMap = null;
            depthToColorMap = null;


        }

        private unsafe ushort[] bmpToUshort(Bitmap bmp)
        {
            System.Drawing.Imaging.BitmapData bData = bmp.LockBits(new Rectangle(new System.Drawing.Point(), bmp.Size),
                System.Drawing.Imaging.ImageLockMode.ReadOnly,
                System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            // number of bytes in the bitmap
            int byteCount = bData.Stride * (bmp.Height);

            short[] bytes = new short[byteCount / 4];
            Marshal.Copy(bData.Scan0, bytes, 0, byteCount / 4);
            bmp.UnlockBits(bData);

            ushort[] returnBytes = (ushort[])(object)(bytes);

            return returnBytes;
        }

        public byte[] imageToByteArray(System.Drawing.Image image)
        {
            using (var ms = new MemoryStream())
            {
                image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                return ms.ToArray();
            }

        }


        /// <summary>
        /// Handles the depth and color frame data arriving from the sensor
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Reader_MultiSourceFrameArrived(object sender, MultiSourceFrameArrivedEventArgs e)
        {
            // Parameters for the chosen buffer
            int aBuf = activeBuffer.value;
            int tmpBufIdx = frameBuffers[aBuf].tempBufferIndex;
            int currTempPos = frameBuffers[aBuf].currentTempPos;

            if (t0.ElapsedMilliseconds > desiredGapBetweenFrames)
            {
                t0.Restart();
                string timestamp;
                if (useTimestampNaming)
                {
                    timestamp = System.DateTime.Now.ToString("hh'-'mm'-'ss'-'ffff", CultureInfo.CurrentUICulture.DateTimeFormat);
                } else
                {
                    timestamp = frameCount.ToString();
                }

                MultiSourceFrame multiSourceFrame = e.FrameReference.AcquireFrame();
                bool framesAvailable = true;

                if (multiSourceFrame != null)
                {
                    if (captureDepthStream)
                    {
                        using (DepthFrame depthFrame = multiSourceFrame.DepthFrameReference.AcquireFrame())
                        {
                            if (depthFrame != null)
                            {
                                if (isRecording)
                                {
                                    // verify data and write the new depth frame data to the display bitmap
                                    if ((depthWidth == this.depthBitmap.PixelWidth) && (depthHeight == this.depthBitmap.PixelHeight))
                                    {
                                        // Copy the pixel data from the image to a temporary array
                                        depthFrame.CopyFrameDataToArray(frameBuffers[aBuf].depthFrameBuffer[tmpBufIdx][currTempPos]);
                                    }
                                }
                            }
                            else
                            {
                                framesAvailable = false;
                                discardedFrames.Add("Depth " + timestamp);
                            }
                        }
                    }

                    using (ColorFrame colorFrame = multiSourceFrame.ColorFrameReference.AcquireFrame())
                    {
                        if (colorFrame != null)
                        {
                            FrameDescription colorFrameDescription = colorFrame.FrameDescription;

                            if ((colorFrameDescription.Width == this.colorBitmap.PixelWidth)
                                && (colorFrameDescription.Height == this.colorBitmap.PixelHeight))
                            {
                                if (isRecording)
                                {
                                    if (colorFrame.RawColorImageFormat == ColorImageFormat.Bgra)
                                    {
                                        colorFrame.CopyRawFrameDataToArray(frameBuffers[aBuf].colorFrameBuffer[tmpBufIdx][currTempPos]);
                                    }
                                    else
                                    {                                        
                                        colorFrame.CopyConvertedFrameDataToArray(frameBuffers[aBuf].colorFrameBuffer[tmpBufIdx][currTempPos], ColorImageFormat.Bgra);
                                    }
                                }

                                if (!isRecording)
                                {
                                    framesAvailable = false;

                                    if (colorFrame.RawColorImageFormat == ColorImageFormat.Bgra)
                                    {
                                        colorFrame.CopyRawFrameDataToArray(this.colorPixels);
                                    }
                                    else
                                    {
                                        colorFrame.CopyConvertedFrameDataToArray(this.colorPixels, ColorImageFormat.Bgra);
                                    }

                                    this.colorBitmap.WritePixels(
                                        new Int32Rect(0, 0, colorFrameDescription.Width, colorFrameDescription.Height),
                                        colorPixels,
                                        colorFrameDescription.Width * this.bytesPerColorPixel,
                                        0);
                                }
                            }
                        }
                        else
                        {
                            framesAvailable = false;
                            discardedFrames.Add("Color " + timestamp);
                        }

                    }

                    // Capture and process the body stream
                    bool dataReceived = false;
                    if (captureBodyStream)
                    {
                        using (BodyFrame bodyFrame = multiSourceFrame.BodyFrameReference.AcquireFrame())
                        {
                            if (bodyFrame != null)
                            {
                                if (this.bodies == null)
                                {
                                    this.bodies = new Body[bodyFrame.BodyCount];
                                }

                                // The first time GetAndRefreshBodyData is called, Kinect will allocate each Body in the array.
                                // As long as those body objects are not disposed and not set to null in the array,
                                // those body objects will be re-used.

                                bodyFrame.GetAndRefreshBodyData(this.bodies);
                                dataReceived = true;
                            }
                        }

                        frameBuffers[aBuf].bodiesFrameBuffer[tmpBufIdx][currTempPos].timestamp = timestamp;

                        if (dataReceived)
                        {
                            int bodyId = 0;

                            foreach (Body body in this.bodies)
                            {
                                if (body.IsTracked)
                                {
                                    CustomBody cBody = new CustomBody(body, bodyId, this.coordinateMapper);

                                    frameBuffers[aBuf].bodiesFrameBuffer[tmpBufIdx][currTempPos].bodies.Add(cBody);
                                }
                                bodyId++;
                            }
                        }
                    }

                    if (framesAvailable)
                    {
                        frameBuffers[aBuf].timestamps[tmpBufIdx][currTempPos] = timestamp;

                        if (!useTimestampNaming)
                        {
                            frameCount++;
                        }
                    }

                    if (framesAvailable && (frameBuffers[aBuf].increaseCurrentTempPos() > 0))
                    {
                        activeBuffer.Increment();
                    }

                }
            }
        }

        private string getCaptureButtonText(string baseText, bool recordingState)
        {
            string buttonText;

            if (recordingState)
            {
                buttonText = Properties.Resources.CaptureRgbDButtonOn;
            }
            else
            {
                buttonText = baseText + "color";

                if (captureDepthStream)
                {
                    buttonText += Properties.Resources.CaptureDepth;
                }

                if (captureBodyStream)
                {
                    buttonText += Properties.Resources.CaptureBody;
                }

                if (captureThermalStream)
                {
                    buttonText += Properties.Resources.CaptureThermal;
                }
            }

            return buttonText;
        }

        private void enableControls(bool state)
        {
            this.generateGenericLookup.IsEnabled = state;
            this.processExistingData.IsEnabled = state;
            //this.captureThermalStreamButton.IsEnabled = state;
            this.captureDepthStreamButton.IsEnabled = state;
            this.captureBodySkeletonsButton.IsEnabled = state;
            this.validateFramesAfterCaptureButton.IsEnabled = state;
            this.enableLosslessCompression.IsEnabled = state;
            this.IncSeqNbr.IsEnabled = state;
            this.ResetSeqNbr.IsEnabled = state;
            this.pathBox.IsEnabled = state;
            this.browseButton.IsEnabled = state;
        }
        

        private void CaptureRgbDButton_Click(object sender, RoutedEventArgs e)
        {
            StartStopCapture();
        }

        private async void StartStopCapture()
        {

            if (!this.kinectSensor.IsOpen)
            {
                this.statusText = Properties.Resources.NoSensorStatusText;
                return;
            }

            if (isRecording)
            {           
                // Stop the recording
                isRecording = false;

                for (int i = 0; i < frameBuffers.Count; ++i)
                {
                    frameBuffers[i].isRecording = false;
                    frameBuffers[activeBuffer.value].setLastFramesRecorded(true);
                }

                // Construct the appropiate text for the capture button
                string buttonText = getCaptureButtonText("Capture ", isRecording);
                this.captureRgbDButton.Content = buttonText;
                this.captureRgbDButton.IsEnabled = false;

                // Wait while the last frames are written to disk
                this.StatusText2 = "Writing last frames to disk...";
                progressBar.Maximum = 100;
                progressBar.IsIndeterminate = false;

                await Task.Run(() => waitAndShow(8000));

                // Validate the capture, if enabled
                if (validateFramesAfterCapture)
                {
                    // Copy the python script to the base folder
                    string filename = "checkFramerate.py";
                    string targetPath = this.captureBaseFolder + "\\" + filename;
                    System.IO.File.Copy(filename, targetPath, true);
                    int allowedTimegap = this.desiredGapBetweenFrames + 50;

                    string capFolder = this.captureBaseFolder + "\\";
                    System.Diagnostics.Process.Start("cmd.exe", "/k cd " + capFolder + " && python checkFramerate.py -f "
                                                       + desiredFrameRate.ToString() + " -g " + allowedTimegap.ToString()
                                                       + " " + captureBaseFolder + "\\" + this.sequenceNbr.ToString() + "\\D");
                }

                if (captureThermalStream)
                {
                    StopThermalRecording();
                }

                // Increment the sequence number to make ready for new capture
                incSeqNbr();

                // Prepare the UI for user input
                this.captureRgbDButton.IsEnabled = true;
                enableControls(true);
                progressBar.IsIndeterminate = false;
                progressBar.Visibility = Visibility.Hidden;
                this.StatusText2 = "Capture finished";
            }
            else
            {
                // Start the recording
                frameCount = 0;

                createFolders();
                for (int i = 0; i < frameBuffers.Count; ++i)
                {
                    frameBuffers[i].sequenceNbr = sequenceNbr;
                }

                this.captureRgbDButton.IsEnabled = false;

                // Enable the progressbar
                progressBar.IsIndeterminate = false;
                progressBar.Visibility = Visibility.Visible;
                
                // Generate intial look-up tables
                coordinateMapper_CoordinateMappingChanged();

                bool startRecording = true;

                if (captureThermalStream)
                {
                    startRecording = StartThermalRecording();
                }

                if (startRecording)
                {
                    this.captureRgbDButton.IsEnabled = true;

                    for (int i = 0; i < frameBuffers.Count; ++i)
                    {
                        frameBuffers[i].captureBaseFolder = this.captureBaseFolder;

                        frameBuffers[i].isRecording = true;
                        frameBuffers[i].setLastFramesRecorded(false);
                    }

                    isRecording = true;

                    // Disable parts of the UI when recording
                    enableControls(false);
                    this.captureRgbDButton.Content = Properties.Resources.CaptureRgbDButtonOn;
                    this.StatusText2 = getCaptureButtonText("Capturing ", false);
                    progressBar.Visibility = Visibility.Visible;
                    progressBar.IsIndeterminate = true;
                }
                else
                {
                    this.StatusText2 = "Thermal recording not available";
                    // Re-enable the button
                    this.captureRgbDButton.IsEnabled = true;

                    // Disable the progressbar
                    progressBar.Visibility = Visibility.Hidden;

                }
            }
        }

        private void waitAndShow(int timeToWait)
        {
            int splitWait = timeToWait/100;

            for (int i = 0; i < 100; ++i)
            {
                Dispatcher.Invoke(delegate()
                {
                    progressBar.Value = i;
                });

                Thread.Sleep(splitWait);
            }
        }


        private void browseButton_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog fDialog = new System.Windows.Forms.FolderBrowserDialog();
            fDialog.SelectedPath = this.pathBox.Text;
            fDialog.Description = "Choose base directory of image files";
            DialogResult result = fDialog.ShowDialog();

            if (System.Windows.Forms.DialogResult.OK == result)
            {
                this.captureBaseFolder = fDialog.SelectedPath;
                this.pathBox.Text = this.captureBaseFolder;
            }
                
        }

        private void ResetSeqNbr_Click(object sender, RoutedEventArgs e)
        {
            this.sequenceNbr = 1;
            this.SeqNbrBox.Text = this.sequenceNbr.ToString();

            if (captureThermalStream)
            {
                ChangeThermalPath();
            }

            createFolders();
        }

        private void IncSeqNbr_Click(object sender, RoutedEventArgs e)
        {
            incSeqNbr();
        }

        private void incSeqNbr()
        {
            this.sequenceNbr++;
            this.SeqNbrBox.Text = this.sequenceNbr.ToString();

            if (captureThermalStream)
            {
                ChangeThermalPath();
            }
            createFolders();
        }

        private void createFolders()
        {
            List<String> modalities = new List<string>();
            modalities.Add("\\D\\");
            modalities.Add("\\RGB\\");
            modalities.Add("\\Bodies\\");
            modalities.Add("\\Lookup\\");
            modalities.Add("\\Lookup\\Generic");

            if (captureThermalStream)
            {
                modalities.Add("\\T\\");
            }

            for (int i = 0; i < modalities.Count; ++i)
            {
                string fullPath = this.captureBaseFolder + "\\" + this.SeqNbrBox.Text + modalities[i];

                bool isExists = System.IO.Directory.Exists(fullPath);

                if (!isExists)
                {
                    System.IO.Directory.CreateDirectory(fullPath);
                }
            }
            

        }

        private async void GenerateGenericLookup_Click(object sender, RoutedEventArgs e)
        {
            string basePath = this.captureBaseFolder + "\\" + this.sequenceNbr.ToString();
            // Create the generic lookup directory
            createFolders();

            progressBar.IsIndeterminate = false;
            progressBar.Visibility = Visibility.Visible;

            this.captureRgbDButton.IsEnabled = false;
            this.generateGenericLookup.IsEnabled = false;
            this.processExistingData.IsEnabled = false;

            await Task.Run(() => generateGenericRegistrationLookupTables(basePath, this.depthWidth, this.depthHeight, 
                                    this.colorWidth, this.colorHeight, this.cbytesPerPixel));

            StatusText2 = "Generic look-up generation complete";
            this.captureRgbDButton.IsEnabled = true;
            this.generateGenericLookup.IsEnabled = true;
            this.processExistingData.IsEnabled = true;
        }

        private async void processExistingData_Click(object sender, RoutedEventArgs e)
        {
            VistaFolderBrowserDialog d = new VistaFolderBrowserDialog();
            d.ShowDialog(this);
            if (d.SelectedPath.ToString().Length > 0)
            {
                // Check if the path is valid
                if (Directory.Exists(d.SelectedPath.ToString() + "\\D") &&
                    Directory.Exists(d.SelectedPath.ToString() + "\\RGB") &&
                    Directory.Exists(d.SelectedPath.ToString() + "\\Lookup"))
                {

                    progressBar.IsIndeterminate = false;
                    progressBar.Visibility = Visibility.Visible;
                    progressBar.Maximum = 1;

                    this.captureRgbDButton.IsEnabled = false;
                    this.generateGenericLookup.IsEnabled = false;
                    this.processExistingData.IsEnabled = false;

                    string folderPath = d.SelectedPath;

                    int tDepthWidth = this.depthWidth;
                    int tDepthHeight = this.depthHeight;
                    int tColorWidth = this.colorWidth;
                    int tColorHeight = this.colorHeight;

                    await Task.Run(() => generateSpecificRegistrationLookupTables(folderPath, tDepthWidth, tDepthHeight, tColorWidth, tColorHeight, this.cbytesPerPixel)).ConfigureAwait(false);
                    StatusText2 = "Generation completed";

                    this.captureRgbDButton.IsEnabled = true;
                    this.generateGenericLookup.IsEnabled = true;
                    this.processExistingData.IsEnabled = true;
                }
                else
                {
                    // Error handling
                    System.Windows.MessageBox.Show("Wrong folder. Should contain D, RGB and Lookup subfolders","Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
        }


        /// <summary>
        /// Handles the event which the sensor becomes unavailable (E.g. paused, closed, unplugged).
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Sensor_IsAvailableChanged(object sender, IsAvailableChangedEventArgs e)
        {
            bool captureStopped = false;

            if (stopCaptureAtConnLoss && !e.IsAvailable && isRecording)
            {
                StartStopCapture();
                captureStopped = true;

            }

            // on failure, set the status text
            this.StatusText = this.kinectSensor.IsAvailable ? Properties.Resources.RunningStatusText
                                                            : Properties.Resources.SensorNotAvailableStatusText;

            if (captureStopped)
            {
                // Show messagebox that we have closed the recording
                System.Windows.MessageBoxResult result = System.Windows.MessageBox.Show("The current capture has ended due to connection loss of the Kinect sensor",
                    "Kinect unavailable", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Asterisk);
            }
        }
       

        private void ValidateFramesAfterCapture_Triggered(object sender, RoutedEventArgs e)
        {
            validateFramesAfterCapture = (sender as CheckBox).IsChecked.Value;
        }

        private void CaptureBody_Triggered(object sender, RoutedEventArgs e)
        {
            captureBodyStream = (sender as CheckBox).IsChecked.Value;


            for (int i = 0; i < frameBuffers.Count; ++i)
            {
                frameBuffers[i].isRecordingBody = captureBodyStream;
            }

            this.captureRgbDButton.Content = getCaptureButtonText("Capture ", isRecording);
        }

        private void CaptureDepthStream_Triggered(object sender, RoutedEventArgs e)
        {
            captureDepthStream = (sender as CheckBox).IsChecked.Value;

            for (int i = 0; i < frameBuffers.Count; ++i)
            {
                frameBuffers[i].isRecordingDepth = captureDepthStream;
            }

            this.captureRgbDButton.Content = getCaptureButtonText("Capture ", isRecording);
        }


        private void CaptureThermalStream_Triggered(object sender, RoutedEventArgs e)
        {
            captureThermalStream = (sender as CheckBox).IsChecked.Value;

            if (captureThermalStream)
            {
                // Open the thermal camera
                StartThermalClient();
            }
            else
            {
                CloseThermalClient();
            }

            this.captureRgbDButton.Content = getCaptureButtonText("Capture ", isRecording);
        }


        private void CloseThermalClient()
        {
            try
            {
                // Close the thermal camera
                thermalClient.CloseMainWindow();
                thermalClient.Kill();
                pipeSender.Dispose();
                pipeReceiver.Dispose();
                pipeExceptionMessageReceiver.Dispose();
                thermalMessageThread.Abort();
            }
            catch (Exception exp)
            {
                Console.WriteLine("Exception when closing the thermal camera: " + exp.Message);
            }
        }

        private void fpsSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            this.desiredFrameRate = (int)e.NewValue;
            this.fpsLabel.Content = desiredFrameRate.ToString() + " fps";

            if (desiredFrameRate == 30)
            {
                // If chosen to 30 fps, let the system run as fast as possible
                this.desiredGapBetweenFrames = 0;
            }
            else
            {
                this.desiredGapBetweenFrames = (1000 / desiredFrameRate) - 5;
            }
        }

        private void enableLosslessCompression_Click(object sender, RoutedEventArgs e)
        {
            bool losslessCompression = (sender as CheckBox).IsChecked.Value;

            for (int i = 0; i < frameBuffers.Count; ++i)
            {
                frameBuffers[i].losslessColorEnabled = losslessCompression;
            }
        }

        private void useTimestampNamingRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.RadioButton ck = (System.Windows.Controls.RadioButton)sender;
            
            useTimestampNaming = ck.IsChecked.Value;
        }

        private void useFrameNumberNamingRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.RadioButton ck = (System.Windows.Controls.RadioButton)sender;

            useTimestampNaming = !ck.IsChecked.Value;
        }
    }


    public class BodiesInFrame
    {
        public BodiesInFrame()
        {
            bodies = new List<CustomBody>();

        }

        public string timestamp;
        public List<CustomBody> bodies;
    }

    public class CustomBody
    {
        public CustomBody()
        {
        }

        public CustomBody(Body body, int bodyId, CoordinateMapper cMapper)
        {
            Joints = new List<CustomJoint>(body.Joints.Count);

            IReadOnlyDictionary<JointType, Joint> joints = body.Joints;
            IReadOnlyDictionary<JointType, JointOrientation> jointOrientations = body.JointOrientations;

            foreach (JointType jointType in joints.Keys)
            {
                // sometimes the depth(Z) of an inferred joint may show as negative
                // clamp down to 0.1f to prevent coordinatemapper from returning (-Infinity, -Infinity)
                CameraSpacePoint position = joints[jointType].Position;

                if (position.Z < 0)
                {
                    position.Z = 0.1F;
                }

                DepthSpacePoint depthSpacePoint = cMapper.MapCameraPointToDepthSpace(position);


                Joints.Add(new CustomJoint(joints[jointType], jointOrientations[jointType], depthSpacePoint));
                TrackingId = body.TrackingId;
                Lean = body.Lean;
                LeanTrackingState = body.LeanTrackingState;
                IsRestricted = body.IsRestricted;
                HandRightState = body.HandRightState;
                HandLeftState = body.HandLeftState;
                HandRightConfidence = body.HandRightConfidence;
                HandLeftConfidence = body.HandLeftConfidence;
                BodyId = bodyId;
            }
        }

        public List<CustomJoint> Joints;
        public ulong TrackingId;
        public int BodyId;
        public Microsoft.Kinect.PointF Lean;
        public TrackingState LeanTrackingState;

        public bool IsRestricted;
        public HandState HandRightState;
        public TrackingConfidence HandRightConfidence;
        public HandState HandLeftState;
        public TrackingConfidence HandLeftConfidence;

        public bool Engaged;
        public FrameEdges ClippedEdges;
    }

    public class BodyWriter
    {
        public void WriteBody(BodiesInFrame bodiesInF, string path)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(BodiesInFrame));
            TextWriter writer = new StreamWriter(path);

            // Serialize the body
            serializer.Serialize(writer, bodiesInF);
            writer.Close();

        }

    }

    public class CustomJoint
    {
        public CustomJoint()
        {

        }

        public CustomJoint(Joint joint, JointOrientation jointOrientation, DepthSpacePoint depthFramePos)
        {
            WorldPosition = joint.Position;
            DepthFramePosition = depthFramePos;

            JointType = joint.JointType;
            TrackingState = joint.TrackingState;
            Orientation = jointOrientation.Orientation;
        }

        public JointType JointType;
        public TrackingState TrackingState;
        public CameraSpacePoint WorldPosition;
        public DepthSpacePoint DepthFramePosition;
        public Vector4 Orientation;
    }

    class FrameBuffer
    {
        public int tempBufferIndex { get; private set; }
        public int diskBufferIndex { get; private set; }
        public int currentTempPos { get; private set; }
        public int currentDiskPos;
        public bool dataToWrite { get; private set; }
        public bool isWriting;
        private bool lastFramesRecorded;
        
        public List<List<ushort[]>> depthFrameBuffer;
        public List<List<byte[]>> colorFrameBuffer;
        public List<List<BodiesInFrame>> bodiesFrameBuffer;
        public List<List<string>> timestamps;
        EventWaitHandle wh = new AutoResetEvent(false);

        public string captureBaseFolder;
        public int sequenceNbr;
        public bool isRecording;
        public bool isRecordingBody;
        public bool isRecordingDepth;

        /// <summary>
        /// Flag deciding if we capture the RGB stram as lossless PNG images
        /// If not, we use 95 % quality level .jpg-compression
        /// </summary>
        public bool losslessColorEnabled;


        private int colorWidth;
        private int colorHeight;
        private int depthHeight;
        private int depthWidth;
        private int bufferLength;

        private BodyWriter bodyWriter;


        /// <summary>
        /// Size of the RGB pixel in the bitmap
        /// </summary>
        private readonly int cbytesPerPixel = (PixelFormats.Gray16.BitsPerPixel + 7) / 8;
        private readonly int bytesPerColorPixel = (PixelFormats.Bgr32.BitsPerPixel + 7) / 8;

        public void init(string captureBaseFolder, int cWidth, int cHeight, int dHeight, int dWidth, int nbrThreads, 
            int bLength, bool isRecordingDepth, bool isRecordingBody, bool losslessColorEnabled)
        {
            // Initialize bufffers
            depthFrameBuffer = new List<List<ushort[]>>();
            colorFrameBuffer = new List<List<byte[]>>();
            bodiesFrameBuffer = new List<List<BodiesInFrame>>();
            timestamps = new List<List<string>>();
            bufferLength = bLength;

            List<ushort[]> tmpDList1 = new List<ushort[]>();
            List<ushort[]> tmpDList2 = new List<ushort[]>();
            List<byte[]> tmpCList1 = new List<byte[]>();
            List<byte[]> tmpCList2 = new List<byte[]>();
            List<string> tmpSList1 = new List<string>();
            List<string> tmpSList2 = new List<string>();
            List<BodiesInFrame> tmpBList1 = new List<BodiesInFrame>();
            List<BodiesInFrame> tmpBList2 = new List<BodiesInFrame>();
            bodyWriter = new BodyWriter();

            this.isRecordingDepth = isRecordingDepth;
            this.isRecordingBody = isRecordingBody;
            this.losslessColorEnabled = losslessColorEnabled;
            this.captureBaseFolder = captureBaseFolder;

            colorWidth = cWidth;
            colorHeight = cHeight;
            depthHeight = dHeight;
            depthWidth = dWidth;

            // Initialize buffers
            for (int i = 0; i < bufferLength; ++i)
            {
                ushort[] tmpDepth1 = new ushort[depthWidth * depthHeight];
                tmpDList1.Add(tmpDepth1);
                ushort[] tmpDepth2 = new ushort[depthWidth * depthHeight];
                tmpDList2.Add(tmpDepth2);

                byte[] tmpColor1 = new byte[colorWidth * colorHeight * this.bytesPerColorPixel];
                tmpCList1.Add(tmpColor1);
                byte[] tmpColor2 = new byte[colorWidth * colorHeight * this.bytesPerColorPixel];
                tmpCList2.Add(tmpColor2);

                BodiesInFrame tmpBodies1 = new BodiesInFrame();
                tmpBList1.Add(tmpBodies1);
                BodiesInFrame tmpBodies2 = new BodiesInFrame();
                tmpBList2.Add(tmpBodies2);
                
                tmpSList1.Add(".tmp");
                tmpSList2.Add(".tmp");
            }

            depthFrameBuffer.Add(tmpDList1);
            depthFrameBuffer.Add(tmpDList2);
            colorFrameBuffer.Add(tmpCList1);
            colorFrameBuffer.Add(tmpCList2);
            bodiesFrameBuffer.Add(tmpBList1);
            bodiesFrameBuffer.Add(tmpBList2);

            timestamps.Add(tmpSList1);
            timestamps.Add(tmpSList2);

            tempBufferIndex = 0;
            diskBufferIndex = 0;
            currentTempPos = 0;
            sequenceNbr = 1;
            dataToWrite = false;
            isWriting = false;

            isRecording = false;
            lastFramesRecorded = false;

            currentDiskPos = 0;
        }

        public int increaseCurrentTempPos()
        {
            currentTempPos++;

            if (currentTempPos >= this.colorFrameBuffer[0].Count)
            {
                this.currentTempPos = 0;

                // Change array
                if (this.tempBufferIndex == 0)
                {
                    this.tempBufferIndex = 1;
                }
                else if (this.tempBufferIndex == 1)
                {
                    this.tempBufferIndex = 0;
                }

                if (isRecording || lastFramesRecorded)
                {
                    dataToWrite = true;
                }

                wh.Set();

                return 1;
            }
            else
            {
                return 0;
            }
        }

        public void setLastFramesRecorded(bool value)
        {
            lastFramesRecorded = value;

            if (value)
            {
                dataToWrite = true;
                wh.Set();
            }
        }

        private int increaseCurrentDiskPos()
        {
            if (isWriting)
            {
                currentDiskPos++;
            }

            if (currentDiskPos >= bufferLength)
            {
                currentDiskPos = 0;

                if (diskBufferIndex == 0)
                {
                    diskBufferIndex = 1;
                } else
                {
                    diskBufferIndex = 0;
                }

                return -1;
            }
            else
            {
                return currentDiskPos;
            }
        }

        

        private int getCurrentDiskPos()
        {
            return currentDiskPos;

        }

        public void save(Object stateInfo)
        {
            while (true)
            {

                if ((!isWriting && dataToWrite) && (lastFramesRecorded || isRecording))
                { 
                    isWriting = true;
                    dataToWrite = false;

                    if (lastFramesRecorded)
                    {
                        lastFramesRecorded = false;
                    }

                    string depthBaseFolder = this.captureBaseFolder + "\\" + this.sequenceNbr.ToString() + "\\D\\" + "D-";
                    string colorBaseFolder = this.captureBaseFolder + "\\" + this.sequenceNbr.ToString() + "\\RGB\\" + "RGB-";
                    string bodiesBaseFolder = this.captureBaseFolder + "\\" + this.sequenceNbr.ToString() + "\\Bodies\\" + "Bodies-";

                    do
                    {
                        if (timestamps[diskBufferIndex][currentDiskPos] != ".tmp")
                        {
                            if (isRecordingDepth)
                            {
                                processDepth(depthFrameBuffer[diskBufferIndex][currentDiskPos], timestamps[diskBufferIndex][currentDiskPos],
                                depthWidth, depthHeight, depthBaseFolder);
                            }

                            if (losslessColorEnabled)
                            {
                                processLosslessColor(colorFrameBuffer[diskBufferIndex][currentDiskPos], timestamps[diskBufferIndex][currentDiskPos],
                                    colorWidth, colorHeight, colorBaseFolder);
                            }
                            else
                            {
                                processColor(colorFrameBuffer[diskBufferIndex][currentDiskPos], timestamps[diskBufferIndex][currentDiskPos],
                                    colorWidth, colorHeight, colorBaseFolder);
                            }

                            if (isRecordingBody)
                            {
                                processBodies(bodiesFrameBuffer[diskBufferIndex][currentDiskPos], bodiesBaseFolder);
                                bodiesFrameBuffer[diskBufferIndex][currentDiskPos].bodies.Clear();
                            }

                            timestamps[diskBufferIndex][currentDiskPos] = ".tmp";
                        }

                        
                    } while (increaseCurrentDiskPos() >= 0);

                    isWriting = false;
                }

                if (!lastFramesRecorded)
                {
                    // Wait one turn before writing any more frames
                    wh.WaitOne();
                }
            }
        }

        private int processColor(Byte[] colorFrameData, string timestamp, int colorWidth, int colorHeight, string colorBaseFolder)
        {            
            string colorPath = colorBaseFolder + timestamp + ".jpg";
            JpegBitmapEncoder colorEncoder = new JpegBitmapEncoder();
            colorEncoder.QualityLevel = 95;

            WriteableBitmap colorBitmap = null;
            colorBitmap = new WriteableBitmap(colorWidth, colorHeight, 96.0, 96.0, PixelFormats.Bgra32, null);

            colorBitmap.WritePixels(
                new Int32Rect(0, 0, colorWidth, colorHeight),
                colorFrameData,
                colorWidth * this.bytesPerColorPixel,
                0);

            colorEncoder.Frames.Add(BitmapFrame.Create(colorBitmap));

            // write the new file to disk
            try
            {
                // FileStream is IDisposable
                using (FileStream fs = new FileStream(colorPath, FileMode.Create))
                {
                    colorEncoder.Save(fs);
                }

            }
            catch (IOException)
            {
                return -1;
            }

            return 0;
        }

        private int processLosslessColor(Byte[] colorFrameData, string timestamp, int colorWidth, int colorHeight, string colorBaseFolder)
        {
            string colorPath = colorBaseFolder + timestamp + ".png";
            PngBitmapEncoder colorEncoder = new PngBitmapEncoder();

            WriteableBitmap colorBitmap = null;
            colorBitmap = new WriteableBitmap(colorWidth, colorHeight, 96.0, 96.0, PixelFormats.Bgra32, null);

            colorBitmap.WritePixels(
                new Int32Rect(0, 0, colorWidth, colorHeight),
                colorFrameData,
                colorWidth * this.bytesPerColorPixel,
                0);

            colorEncoder.Frames.Add(BitmapFrame.Create(colorBitmap));

            // write the new file to disk
            try
            {
                // FileStream is IDisposable
                using (FileStream fs = new FileStream(colorPath, FileMode.Create))
                {
                    colorEncoder.Save(fs);
                }

            }
            catch (IOException)
            {
                return -1;
            }

            return 0;
        }


        private int processDepth(ushort[] depthFrameData, string timestamp, int depthWidth, int depthHeight, string depthBaseFolder)
        {
            string depthPath = depthBaseFolder + timestamp + ".png";
            BitmapEncoder depthEncoder = new PngBitmapEncoder();

            WriteableBitmap depthBitmap = null;
            depthBitmap = new WriteableBitmap(depthWidth, depthHeight, 96.0, 96.0, PixelFormats.Gray16, null);

            depthBitmap.WritePixels(
                new Int32Rect(0, 0, depthWidth, depthHeight),
                depthFrameData,
                depthWidth * this.cbytesPerPixel,
                0);

            depthEncoder.Frames.Add(BitmapFrame.Create(depthBitmap));


            // write the new file to disk
            try
            {
                // FileStream is IDisposable
                using (FileStream fs = new FileStream(depthPath, FileMode.Create))
                {
                    depthEncoder.Save(fs);
                }

            }
            catch (IOException)
            {
                return -1;
            }

            return 0;

        }

        private int processBodies(BodiesInFrame bodies, string bodiesBaseFolder)
        {
            string bodiesPath = bodiesBaseFolder + bodies.timestamp + ".xml";

            try
            {
                bodyWriter.WriteBody(bodies, bodiesPath);

            }
            catch
            {
                return -1;
            }

            return 0;
        }

    }

    class CyclicCounter
    {
        public int value { get; private set; }

        private int max;

        public CyclicCounter(int maxVal)
        {
            max = maxVal;
            value = 0;
        }

        public int Increment()
        {
            value++;

            if (value >= max)
            {
                value = 0;
            }

            return value;
        }


    }
}
