from __future__ import division
import os
import glob
import string
import argparse
from datetime import timedelta
 
if __name__ == '__main__':
		parser = argparse.ArgumentParser(description='Calculate frame rate consistency')
	   
		parser.add_argument('-e', '--extension', metavar='png', default='png', type=str, help='File name extension of the images that should be searched for.')
		parser.add_argument('-f', '--framerate', metavar='25', default='25', type=int, help='Minimum frame rate. If the frame rate is equal or below this value, the entry will be highlighted')
		parser.add_argument('-g','--timegap',metavar='80', default='80', type=int, help='Maximum time gap between consecutive frames. If the time gap is equal to or above this value, the entry will be highlighted')
		parser.add_argument('searchDir', metavar='dataDir', type=str, help='The search directory. Relative to the directory from where the script is run.')
		args = parser.parse_args()
	   
		files = glob.glob(os.path.join(".", args.searchDir, "*.%s" % (args.extension)))
		filepath = os.path.abspath(os.path.join(".", args.searchDir))
		
		print("*") * 10
		print("Analysing files in %s \n" % filepath)
		
		files.sort()
 
		remainder = [string.rsplit(x, '-', 1)[0] for x in files]
		msecsfileext = [string.rsplit(x, '-', 1)[1] for x in files]
		
		msecs = [int(string.rsplit(x, '.', 1)[0])*100 for x in msecsfileext]
		
		secs = [int(string.rsplit(x, '-', 2)[1]) for x in files]
		min = [int(string.rsplit(x, '-', 3)[1]) for x in files]
		hourString = [string.rsplit(x, '-', 4)[1] for x in files]
		hour = [int(string.rsplit(x, '\\')[0]) for x in hourString]
 		 
		d = {}
		[d.__setitem__(f,1+d.get(f,0)) for f in remainder]
 
		print("All values:")
		print(d.values())
		print("\nMean: %f" % (sum(d.values())/len(d.values())))
 
		print("Problems at:")
 
		output = []
 
		for k, v in d.iteritems():
				if v < args.framerate:
						output.append("\t%s: %d frames" % (k, v));
 
		output.sort()
 
		for text in output:
				print(text)
			
		timegapOutput = []
		
		# Find problematic time gaps between instances
		for i in range(1,len(min)):
			prevTime = timedelta(hours = hour[i-1], minutes = min[i-1],
								seconds = secs[i-1], microseconds = msecs[i-1])
			currTime = timedelta(hours = hour[i], minutes = min[i],
								seconds = secs[i], microseconds = msecs[i])
								
			timeGap = currTime - prevTime
			
			timeGapInMs = timeGap.microseconds/1000 + timeGap.seconds*1000;
			
			
			if timeGapInMs > args.timegap and timeGap.seconds != 3600:
				timegapOutput.append("\t%s\n\t%s:\t%d sec %d msecs" % (files[i-1],files[i], timeGap.seconds, timeGap.microseconds/1000))
	
		
		if timegapOutput:
			print("Gap between")
			for timegapText in timegapOutput:
				print(timegapText)
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			